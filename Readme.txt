=====================================================================================================================================

Ce programme ne s'utilise qu'avec des fichiers .docx, pour l'utiliser avec des .doc, veuillez simplement enregistrer ses derniers en .docx.

/!\ Important votre fichier ne dois pas contenir d'ent�te ou de pied de page. Si vous en avez, retirez les avant de convertir votre fichier.

=====================================================================================================================================

Utilisation :

	- Lancer l'executable Word2Wiki v3.exe .
	- Une fen�tre de selection de fichier va s'ouvrir.
	- S�lectionner 1 ou plusieurs fichiers � convertir dans la fen�tre de selection.
	- Un dossier sera cr�� pour chaque fichiers s�lection�s avec en nom celui du fichier correspondant.
	- Dans chaque dossier sera pr�sent :
		- Un fichier "Texte.txt", il s'agit du code WikiCode � coller  dans l'espace "modifier le wikicode" de votre Wiki.
		- Un dossier "Images", il s'agit du dossier contenant les images � t�l�verser sur votre Wiki.

=====================================================================================================================================

RIDEL Marius - Juillet 2018 - Langage utilis�e : AutoIt

=====================================================================================================================================