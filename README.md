
Ce programme ne s'utilise qu'avec des fichiers .docx, pour l'utiliser avec des .doc, veuillez simplement enregistrer ses derniers en .docx.

/!\ Important votre fichier ne dois pas contenir d'entête ou de pied de page. Si vous en avez, retirez les avant de convertir votre fichier.

Utilisation :

	- Lancer l'executable Word2Wiki v3.exe .
	- Une fenêtre de selection de fichier va s'ouvrir.
	- Sélectionner 1 ou plusieurs fichiers à convertir dans la fenêtre de selection.
	- Un dossier sera créé pour chaque fichiers sélectionés avec en nom celui du fichier correspondant.
	- Dans chaque dossier sera présent :
		- Un fichier "Texte.txt", il s'agit du code WikiCode à coller  dans l'espace "modifier le wikicode" de votre Wiki.
		- Un dossier "Images", il s'agit du dossier contenant les images à téléverser sur votre Wiki.



RIDEL Marius - Juillet 2018 - Langage utilisée : AutoIt
