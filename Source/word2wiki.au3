#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Outfile_x64=Word2Wiki v6.exe
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#include <FileConstants.au3>
#include <MsgBoxConstants.au3>
#include <AutoItConstants.au3>
#include <WinAPIFiles.au3>
#include <Array.au3>
#include <String.au3>
#include <File.au3>
#include "Include/Zip.au3"

#cs
# @Author:	RIDEL Marius
# @Date:	11 July 2018
# @Brief:	Convert .docx files into a WikiCode .txt + Images folder
# @Use :	Select one or multi .docx file ( /!\ those files must not have header/footer ).
# 			Separate folders are created for each files with a Texte.txt doc and an "Images" folder inside.
#			Texte.txt contain the WikiCode text and the Images folder contain the images to upload.
#ce

;Nombre maximum d'images dans un document
 Local $maxImageInDoc = 1000

;Sélection du/des fichiers
 Local $fichierChoisi = FileOpenDialog("Veuillez sélectionner un ou plusieurs fichiers", @ScriptDir & "\", "Document Word (*.docx)", 5)

 If @error Then

	 MsgBox(48, "Erreur", "Aucun fichier sélectionné.")

 Else

	;Séparation au cas où il y ait plusieurs fichiers sélectionnés
	 $multiFichiers = StringSplit($fichierChoisi, "|")

	 $nombreFichier = 1
	 If(UBound($multiFichiers) > 2) Then $nombreFichier = 2

	 For $i = $nombreFichier To $multiFichiers[0]

		;Initialisation des variables pour les images
		 Local $numImg = 1
		 Local $ListImages[$maxImageInDoc]
		 $ListImages[0]=0

		;Sélection du fichier à analyser
		 Local $fichierChoisi = $multiFichiers[$i]
		 If($nombreFichier == 2) Then $fichierChoisi = $multiFichiers[1]&'\'&$multiFichiers[$i]

		;Initialisation des variables du fichier choisi
		 Local $cheminFichier = StringTrimRight($fichierChoisi,5)
		 Local $nomFichier = StringTrimLeft( $cheminFichier, StringInStr( $fichierChoisi, "\", 0, -1) )

		;Transformation du .docx en .zip et décompression du .zip
		 FileCopy($fichierChoisi,$fichierChoisi&".zip")
		 _Zip_UnzipAll($fichierChoisi&".zip", $cheminFichier&"\")
		 FileDelete($fichierChoisi&".zip")

		;Lecture du contenu du fichier XML
		 Local $fichierXML = FileOpen($cheminFichier&"\word\document.xml")
		 Local $contFichier = StringReplace(FileRead($fichierXML),'<w:t xml:space="preserve">',"<w:t>")
		 $contFichier = StringReplace($contFichier,'<w:r w',"<w:r>w")
		 $contFichier = StringReplace($contFichier,'*','<nowiki>*</nowiki>')
		 FileClose($fichierXML)

		 ;Lecture du contenu du fichier des relations XML
		 Local $fichierRelsXML = FileOpen($cheminFichier&"\word\_rels\document.xml.rels")
		 Local $contFichierRels = FileRead($fichierRelsXML)
		 FileClose($fichierRelsXML)

		 ;Lecture du contenu du fichier des puces et numérotations XML
		 Local $fichierPuce = FileOpen($cheminFichier&"\word\numbering.xml")
		 Local $contFichierPuce = FileRead($fichierPuce)
		 FileClose($fichierPuce)
		 $contFichierPuce = StringSplit( $contFichierPuce, '<w:num w:numId=', 1)
		 Local $tabPuce[$contFichierPuce[0] -1][2]
		 For $j = 2 To $contFichierPuce[0]
			 Local $numIDPuce = StringMid($contFichierPuce[$j],1, StringInStr($contFichierPuce[$j],'"',1,2) )
			 Local $abstractIDPuce = '<w:abstractNum w:abstractNumId=' & StringMid($contFichierPuce[$j], StringInStr($contFichierPuce[$j],'w:val="',1)+6, StringInStr($contFichierPuce[$j],'"/>',1) - (StringInStr($contFichierPuce[$j],'w:val="',1)+5) )
			 Local $searchString = StringMid($contFichierPuce[1],StringInStr($contFichierPuce[1], $abstractIDPuce,1))
			 $searchString = StringMid($searchString,StringInStr($searchString, '<w:numFmt w:val="',1) + 17)
			 Local $typePuce = StringMid($searchString,1,StringInStr($searchString,'"',1)-1)
			 $tabPuce[$j-2][0] = $numIDPuce
			 $tabPuce[$j-2][1] = $typePuce
		 Next

		;Initialisation de la variable de balise ( $balise[0] : préfixe; $balise[1] : suffixe )
		 Local $balise = ""

		;Gestion des cadres
		 If (StringInStr($contFichier,'<wps:wsp>',1) <> 0) Then

			 Local $lesCadres = StringSplit($contFichier, '<wps:wsp>',1)

			 For $j = 2 To $lesCadres[0]

				 $lesCadres[$j] = StringMid($lesCadres[$j],1 ,StringInStr($lesCadres[$j],'</wps:wsp>',1)+9)

				 $contFichier = StringReplace($contFichier, $lesCadres[$j], "")

			 Next

		 EndIf

		;Séparation du contenu du fichier en paragraphes ( $Paragraphes[0] : nombre de paragraphes )
		 Local $Paragraphes = StringSplit($contFichier, "w:rsidP=",1)

		 For $j = 1 To $Paragraphes[0]

			;Génération de la balise pour le paragraphe courant
			 Global $estTitre = False
			 Global $estTableau = False
			 Global $estPuce = False
			 $balise = choixBalise($Paragraphes[$j])

			;Séparation des paragraphes en mots ( $Mots[0] : nombre de mots )
			 Local $Mots = StringSplit($Paragraphes[$j], "<w:r>",1)

			 For $k = 1 To $Mots[0]

				 Local $italique = False
				 Local $gras = False
				 Local $souligne = False

				 If (StringInStr($Mots[$k],'<w:t>',1) <> 0) Then
					;Pour l'italique
					 If (StringInStr($Mots[$k],'<w:i/>',1) <> 0) Then
						 $italique = True
					 EndIf

					 ;Pour le gras
					 If (StringInStr($Mots[$k],'<w:b/>',1) <> 0) Then
						 $gras  = True
					 EndIf

					 ;Pour le souligné
					 If (StringInStr($Mots[$k],'<w:u',1) <> 0) Then
						 $souligne = True
					 EndIf
				 EndIf

				;Suppression des balises XML inutiles
				 $Mots[$k]= StringReplace( $Mots[$k],StringMid( $Mots[$k], 1, StringInStr( $Mots[$k],'<w:t>',1)+4 ),"")

				 $Mots[$k]= StringReplace( $Mots[$k],StringMid( $Mots[$k], StringInStr( $Mots[$k],'</w:t>',1) ),"")

				 If ($italique) Then $Mots[$k]= "''"&$Mots[$k]&"''"
				 If ($gras) Then $Mots[$k]= "'''"&$Mots[$k]&"'''"
				 If ($souligne) Then $Mots[$k]= "<u>"&$Mots[$k]&"</u>"

			 Next

			 ;Insertion des préfixes et suffixes
			 $Mots[1] &= $balise[0]
			 $Mots[UBound($Mots)-1] &= $balise[1]
			 If( Not($estPuce) Or Not($estTableau) Or Not($estTitre) ) Then $Mots[UBound($Mots)-1] &=@CRLF

			;Recomposition du paragraphe
			 $Paragraphes[$j] = _ArrayToString($Mots, "",1)



		 Next

		;Recomposition du contenu du fichier
		 $Paragraphes = _ArrayToString($Paragraphes,"",2)

		;Listing des images ( $imgList[0] =  Nombres d'images)
		 $imgList = StringSplit($contFichierRels, '<Relationship Id="',1)


		;Association IdXML/Image réelle

		 Local $AssociationImg[$ListImages[0] + 1]

		 For $j = 2 to $imgList[0]

			Local $XMLrId = StringMid($imgList[$j],1,StringInStr( $imgList[$j],'"',1) -1)

			For $k = 1 to $ListImages[0]

				If ( $ListImages[$k] == $XMLrId) Then

					Local $deb = StringMid($imgList[$j],StringInStr( $imgList[$j],'media/',1) + 6)
					Local $nomReel = StringMid($deb,1,StringInStr( $deb,'"',1)-1)
					$AssociationImg[$k] = $nomReel

				EndIf

			Next

		 Next

		 ;Sauvegarde et nettoyage de la liste d'images.
		 For $j = 1 To $ListImages[0]

			;Copie et renommage des images dans un dossier temporaire
			 FileCopy($cheminFichier&"\word\media\"&$AssociationImg[$j], StringReplace($cheminFichier,$nomFichier,"")&"\ImagesTemp\"&$nomFichier&"-"&$AssociationImg[$j], 9)

			;Remplace dans le contenu les ID des images par leur nom réel
			 $Paragraphes = StringReplace($Paragraphes,$ListImages[$j]&"]]", "File:"&$nomFichier&"-"&$AssociationImg[$j]&"]]")

			 $ListImages[$j] = ""

		 Next

		;Suppression des fichiers inutiles
		 DirRemove($cheminFichier, 1)

		;Génération des fichiers finaux
		 DirMove(StringReplace($cheminFichier,$nomFichier,"")&"\ImagesTemp", $cheminFichier&"\Images", 1)
		 FileWrite($cheminFichier&"\Texte.txt",$Paragraphes)

	 Next
	 MsgBox(64,"Youpi !","La conversion s'est terminée")
 EndIf

 Func choixBalise($string) ;Retourne les caractères wikicodes qui entourent le texte + les balises d'images

	 Local $leretour[2]
	 $leretour[0] = "" ;Préfixe
	 $leretour[1] = @CRLF ;Suffixe

	 $estTitre = False
	 $estTableau = False
	 $estPuce = False
	 Local $poschar = 0

	;Gestion des tableaux
	Local $debTab = False

	 ;Début tableau
	 $poschar = StringInStr($string,'<w:tbl>',1)
     If ($poschar <> 0) Then
		 $leretour[1] &='{| class="wikitable alternance center"'&@CRLF
		 $debTab = True
		 $estTableau = True
	 EndIf

	 ;Fin tableau
	 $poschar = StringInStr($string,'</w:tbl>',1)
     If ($poschar <> 0) Then
		 $leretour[1] &= "|}"
		 $estTableau = True
	 EndIf

	;Ligne
	 $poschar = StringInStr($string,'<w:tr ',1)
     If ($poschar <> 0) Then
		 If ( $debTab == false ) Then $leretour[1] &= "|-" &@CRLF
		 $debTab = false
		 $estTableau = True
     EndIf

	 ;Colonne
	 $poschar = StringInStr($string,'<w:tc>',1)
	 If ($poschar <> 0) Then
		 $estTableau = True
		Local $specialLine = False

		If(StringInStr($string,"<w:vMerge/>",1) == 0) Then $leretour[1] &= "| "

		;Colonne fusionnée
		$poschar = StringInStr($string,'<w:gridSpan w:val="',1)
		If($poschar <> 0) Then
			$leretour[1] &= 'colspan=' & StringMid($string,$poschar + 18,3) & ' '
			$specialLine = True
		EndIf

		;Ligne fusionnée
		$poschar = StringInStr($string,'<w:vMerge w:val="restart"/>',1)
		If($poschar <> 0) Then

			Local $searchString = StringMid($contFichier, StringInStr($contFichier,$string,1) + $poschar + 25)
			Local $finSearch = StringInStr($searchString,'</w:tbl>',1)
			If ( $poschar = StringInStr($searchString,'<w:vMerge w:val="restart"/>',1) <>0) Then $finSearch = $poschar
			$searchString = StringMid($searchString, 1, $finSearch)

			StringReplace($searchString, "<w:vMerge/>", "<w:vMerge/>")
			Local $nbOccurences = @extended

			$leretour[1] &= 'rowspan="' & $nbOccurences & '" '
			$specialLine = True
		EndIf

		If( $specialLine == True ) Then $leretour[1] &= '| '
	 EndIf

	 ;Pour les titres
	 $poschar = StringInStr($string,'w:val="Titre',1)
     If ($poschar <> 0) Then
		 $estTitre = True
		 $leretour[0] = @CRLF &  $leretour[0] & _StringRepeat("=",1+StringMid($string,$poschar + 12,1))
		 $leretour[1] = _StringRepeat("=",1+StringMid($string,$poschar + 12,1)) & $leretour[1] & @CRLF
	 EndIf

	 ;Pour les puces et la numérotation
	 $poschar = StringInStr($string,'<w:numId w:val="',1)
	 If ($poschar <> 0 And $estTitre == False ) Then
		 $estPuce = True
		 Local $numIDPuce = StringMid($string, $poschar + 15, StringInStr(StringMid($string,$poschar + 16),'"',1)+1)
		 Local $typePuce = $tabPuce[_ArraySearch($tabPuce,$numIDPuce, 0, 0, 0, 1, 1, 0)][1]
		 If($typePuce = "bullet") Then
			 $typePuce = "*"
		 Else
			 $typePuce = "#"
		 EndIf

		 Local $niveauPuce = StringMid($string,StringInStr($string,'<w:ilvl w:val="',1)+ 15,StringInStr(StringMid($string,StringInStr($string,'<w:ilvl w:val="',1)+15),'"',1) - 1 )

		 $leretour[0] &= _StringRepeat($typePuce, $niveauPuce+1)
		 $leretour[1] = "" & $leretour[1]
	 EndIf

	 ;Pour les images
	 $poschar = StringInStr($string,'<a:blip r:embed="',1)
	 If ($poschar <> 0) Then

		 Local $images = StringSplit($string, '<a:blip r:embed="',1)

		 For $i = 2 To $images[0]

			 $rId = StringMid($images[$i],1,StringInStr( $images[$i],'"',1)-1)

			 If(_ArraySearch($ListImages,$rId,0,$ListImages[0],1) = -1) Then
				 $ListImages[0] += 1
				 $ListImages[$ListImages[0]] = $rId
			 EndIf

			 $images[$i] = "[["&$rId&"]]"

		 Next

		 $leretour[0] &= _ArrayToString($images, "",2)
		 $leretour[1] = "" & $leretour[1]

	 EndIf

	 return $leretour

 EndFunc